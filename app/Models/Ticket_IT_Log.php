<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket_IT_Log extends Model
{
    use HasFactory;

    protected $table = 'ticket_it_log';

}

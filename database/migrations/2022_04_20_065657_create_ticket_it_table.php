<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_it', function (Blueprint $table) {
            $table->id();
            $table->string('urf_no');
            $table->string('user_fullname');
            $table->string('user_dept');
            $table->string('user_email');
            $table->string('user_ext');
            $table->string('requestor_fullname')->nullable();
            $table->string('requestor_dept')->nullable();
            $table->string('requestor_email')->nullable();
            $table->string('requestor_ext')->nullable();
            $table->string('request_type')->nullable();
            $table->string('requirement_type')->nullable();
            $table->string('application')->nullable();
            $table->string('others')->nullable();
            $table->string('criticality')->nullable();
            $table->string('description')->nullable();
            $table->string('required_completion_date')->nullable();
            $table->string('required_owner_approval')->nullable();
            $table->string('urf_status')->nullable();
            $table->string('sendtoHOU')->nullable();
            $table->string('sendtoHOD')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
            $table->string('updated_by')->nullable();
            $table->string('assignTo')->nullable();
            $table->timestamp('assigned_at')->nullable();
            $table->string('officer')->nullable();
            $table->string('requirement_type_it')->nullable();
            $table->string('others_it')->nullable();
            $table->string('complexity')->nullable();
            $table->string('date_ictd_testing')->nullable();
            $table->string('date_user_testing')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_it');
    }
};
